import Ember from 'ember';

export default Ember.Route.extend({

  session: Ember.inject.service(),

  actions: {
    authenticate(login, password) {
      this.get('session').authenticate('authenticator:oauth2', login, password).catch((reason) => {
        this.set('errorMessage', reason.error || reason);
      });
    },

    authenticateFb() {
      this.get('session').authenticate('authenticator:torii', 'facebook');

    },
  }

});
