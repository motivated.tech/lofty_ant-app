import FacebookOauth2Provider from 'torii/providers/facebook-oauth2';
import Ember from 'ember';

export default FacebookOauth2Provider.extend({
  session: Ember.inject.service(),
  fetch(data) {
    return data;
  }
});
