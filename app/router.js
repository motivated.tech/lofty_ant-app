import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('course', {path: '/course/:course_id'});
  this.route('courses');
  this.route('group', {path: '/group/:group_id'});
  this.route('groups');
  this.route('login');
});

export default Router;
